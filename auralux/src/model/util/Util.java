package model.util;

import java.util.Random;

public class Util {
	
	/*
	 * literal utility class for stuffs
	 */
	
	// random number with min max support. Includes max (as it should)
	static Random random = new Random();
	public static int random(int min, int max){
		return random.nextInt(max - min + 1) + min;
	}
	
	// translate a number from one range to another
	// TODO: verify if this is still needed
	public static double translateToRange(double input, double inputMin, double inputMax, double outputMin, double outputMax) {
		return ( ( (input - inputMin) * (outputMax - outputMin) ) / (inputMax - inputMin) ) + outputMin;
	}
	public static double translateToRange(int input, int inputMin, int inputMax, int outputMin, int outputMax) {
		return ( ( (input - inputMin) * (outputMax - outputMin) ) / (inputMax - inputMin) ) + outputMin;
	}
	
	// normalizes a vector to a range of 0-359 (probably)
	public static double normalizeAngle360(double angle) {
		return (angle > 360) ? angle-360 : (angle < 0) ? angle : angle;
	}
	
	public static double normalizeAngle360(int angle) {
		return (angle > 360) ? angle-360 : (angle < 0) ? angle : angle;
	}
	
}
