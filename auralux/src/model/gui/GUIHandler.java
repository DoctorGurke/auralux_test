package model.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import model.util.Util;


public class GUIHandler {
	
	private LinkedList<GUIElement> elements = new LinkedList<GUIElement>();
	
	BufferedImage backgroundImage;
	
	public GUIHandler() {
		File backgroundImageFile;
		try {
			backgroundImageFile = new File(getClass().getResource("/resource/image/skybox_" + Util.random(1, 18) + ".png").toURI());
			backgroundImage = ImageIO.read(backgroundImageFile);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public LinkedList<GUIElement> getElements() {
		return this.elements;
	}
	
	// adds an element to the gui handler for gui handling
	public void addObject(GUIElement element) {
		this.elements.add(element);
	}
}
