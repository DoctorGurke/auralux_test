package model;

import java.util.LinkedList;

import model.ents.Sun;
import model.input.SelectionBox;

public class WinCondition {
	
	private static LinkedList<Sun> tracking = new LinkedList<Sun>();
	
	
	public static void addToTracking(Sun sun) {
		WinCondition.tracking.add(sun);
	}
	
	public static void resetTracking() {
		WinCondition.tracking.clear();
	}
	
	public static void updateNode() {
		int otherTeam = 0;
		int playerTeam = 0;
		for (int i = 0; i < tracking.size(); i++) {
			Sun sun = tracking.get(i);
			if (sun.getTeam() == Player.getTeam())
				playerTeam++;
			else if (sun.getTeam() == null) {
				
			} else if (sun.getTeam() != Player.getTeam())
				otherTeam++;
		}
		if (playerTeam <= 0 && ( Game.getGameState() == Game.GAME_STATE_RUNNING || Game.getGameState() == Game.GAME_STATE_PAUSED) ) {
			System.out.println("player loss");
			Game.setGameState(Game.GAME_STATE_LOSS);
			SelectionBox.clear();
		}
		else if (otherTeam <= 0 && ( Game.getGameState() == Game.GAME_STATE_RUNNING || Game.getGameState() == Game.GAME_STATE_PAUSED) ) {
			System.out.println("player win");
			Game.setGameState(Game.GAME_STATE_WIN);
			SelectionBox.clear();
		}
	}
	
}
