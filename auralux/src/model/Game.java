package model;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

import model.ents.*;
import model.gui.GUIElement;
import model.gui.GUIHandler;
import model.input.KeyInput;
import model.input.MouseInput;
import model.input.MouseMotionInput;
import model.input.SelectionBox;
import model.spatial.MapGrid;
import view.RenderHandler;
import view.Window;

public class Game extends Canvas implements Runnable{
	
	private static final long serialVersionUID = -696110484441605994L;
	
	// used for menu state and game states
	private static int gamestate = 0;
	public static final int GAME_STATE_MAIN_MENU = 0; // Main start menu
	public static final int GAME_STATE_RUNNING = 1; // running game
	public static final int GAME_STATE_PAUSED = 2; // paused running game
	public static final int GAME_STATE_GAME_MENU = 3; // paused running game menu
	public static final int GAME_STATE_COLOUR_SELECT = 4; // pre-game start colour selection
	public static final int GAME_STATE_MAP_SELECT = 5; // pre-game start map select
	public static final int GAME_STATE_LOGIN = 6; // login?
	public static final int GAME_STATE_SETTINGS = 7; // settings?
	public static final int GAME_STATE_WIN = 8; // win screen post-game
	public static final int GAME_STATE_LOSS = 9; // loss screen post-game
	
	// for future implementation of slow/faster game speed?
	private static double gameScale = 1;
	
	// later used for debug mode maybe
	// TODO: implement it lol
	//private boolean debug = false;

	private final static Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
	private final static Dimension aspectratio = getAspectRatio();
	public static final int WIDTH = (int) (screensize.getWidth() - 70), HEIGHT = (int) (WIDTH / aspectratio.getWidth() * aspectratio.getHeight());
	public final int PREFERRED_TPS = 60; // default tps
	
	private static Thread thread;
	private static boolean running = false;
	private static int fps = 0;
	private static int tps = 0;
	private static double frametime = 0;
	
	private static ArrayList<Team> teams = new ArrayList<Team>();
	
	private static GameHandler handler;
	private static RenderHandler renderHandler;
	private static GUIHandler guihandler;
	private static Window window;
	
	public static final int MAX_MAP_WIDTH = 4096, MAX_MAP_HEIGHT = 4096;
	
	private static int activeMap = 1; // default map
	
	public Game() {
		handler = new GameHandler();
		renderHandler = new RenderHandler();
		guihandler = new GUIHandler();
		this.addKeyListener(new KeyInput());
		this.addMouseListener(new MouseInput());
		this.addMouseMotionListener(new MouseMotionInput());
		// camera position -(x) + WIDTH, -(y) + HEIGHT
		Camera camera = new Camera( -(MAX_MAP_WIDTH / 2) + WIDTH, -(MAX_MAP_HEIGHT / 2) + HEIGHT);
		window = new Window(WIDTH, HEIGHT, "SpaceTacos", this, camera);
		
		try {
		     GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		     ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("src/resource/font/Electrolize-Regular.ttf")));
		} catch (Exception e) {
		     e.printStackTrace();
		}
		
		new Team("green", Color.green);
		new Team("magenta", Color.magenta);
		new Team("purple", new Color(128, 0, 255));
		new Team("yellow", Color.yellow);
		new Team("orange", new Color(255, 128, 0));
		new Team("red", Color.red);
		new Team("blue", Color.blue);
		
		Player.setTeam(getTeamByName("red"));
		
		// my very yanky custom GUI system
		// it is really bad and super unintuitive but it actually works better than expected
		
		// main menu
		guihandler.addObject(new GUIElement(Game.GAME_STATE_MAIN_MENU, GUIElement.ACTION_MAP_SELECT_MENU, WIDTH / 2 - 250, HEIGHT / 2 - 20 - 80, 500, 80, "START GAME", true));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_MAIN_MENU, GUIElement.ACTION_EXIT, WIDTH / 2 - 200, HEIGHT / 2 + 20, 400, 80, "QUIT GAME"));
		
		// map select menu
		guihandler.addObject(new GUIElement(Game.GAME_STATE_MAP_SELECT, GUIElement.ACTION_MAP_1, 0, 0, 100, 100, "MAP 1"));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_MAP_SELECT, GUIElement.ACTION_MAP_2, 150, 0, 100, 100, "MAP 2"));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_MAP_SELECT, GUIElement.ACTION_MAP_3, 300, 0, 100, 100, "MAP 3"));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_MAP_SELECT, GUIElement.ACTION_MAP_4, 450, 0, 100, 100, "MAP 4"));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_MAP_SELECT, GUIElement.ACTION_MAP_5, 600, 0, 100, 100, "MAP 5"));
		
		// colour select menu
		guihandler.addObject(new GUIElement(Game.GAME_STATE_COLOUR_SELECT, GUIElement.ACTION_COLOR_GREEN, 0, 150, 100, 100, "", Color.green, Color.green));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_COLOUR_SELECT, GUIElement.ACTION_COLOR_ORANGE, 150, 150, 100, 100, "", new Color(255, 128, 0), new Color(255, 128, 0)));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_COLOUR_SELECT, GUIElement.ACTION_COLOR_MAGENTA, 300, 150, 100, 100, "", Color.magenta, Color.magenta));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_COLOUR_SELECT, GUIElement.ACTION_COLOR_PURPLE, 450, 150, 100, 100, "", new Color(128, 0, 255), new Color(128, 0, 255)));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_COLOUR_SELECT, GUIElement.ACTION_COLOR_BLUE, 600, 150, 100, 100, "", Color.blue, Color.blue));
		
		// game paused menu
		guihandler.addObject(new GUIElement(Game.GAME_STATE_GAME_MENU, GUIElement.ACTION_TOGGLE_GAME, WIDTH / 2 - 200, HEIGHT / 2 - 20 - 80, 400, 80, "RESUME GAME"));
		guihandler.addObject(new GUIElement(Game.GAME_STATE_GAME_MENU, GUIElement.ACTION_MAIN_MENU, WIDTH / 2 - 200, HEIGHT / 2 + 20, 400, 80, "BACK TO MENU"));
	
		// game win
		guihandler.addObject(new GUIElement(Game.GAME_STATE_WIN, GUIElement.ACTION_MAIN_MENU, WIDTH / 2 - 250, HEIGHT / 2 + 50, 500, 80, "RETURN TO MAIN MENU"));
		
		// game win
		guihandler.addObject(new GUIElement(Game.GAME_STATE_LOSS, GUIElement.ACTION_MAIN_MENU, WIDTH / 2 - 250, HEIGHT / 2 + 50, 500, 80, "RETURN TO MAIN MENU"));
	}
	
	/*
	 * start an actual running game/map
	 * TODO: implement loading of different maps via database (UUUUGHHHH)
	 */
	public static void startGame() {
		if (!(Game.activeGame())) {
			switch (Game.activeMap) {
			// map 1
			case 1:
				Window.getCamera().setX(-MAX_MAP_WIDTH / 2 + WIDTH);
				Window.getCamera().setY(-MAX_MAP_HEIGHT / 2 + HEIGHT);
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 - 600, Player.getTeam()));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 - 200));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 200));
				// AI
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 600, getTeamByName("red")));
				break;
				
			// map 2
			case 2:
				Window.getCamera().setX(-MAX_MAP_WIDTH / 2 + WIDTH);
				Window.getCamera().setY(-MAX_MAP_HEIGHT / 2 + HEIGHT);
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 - 400, Player.getTeam()));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 - 200, MAX_MAP_HEIGHT / 2));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 + 200, MAX_MAP_HEIGHT / 2));
				// AI
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 400, getTeamByName("red")));
				break;
			
			// map 3
			case 3:
				Window.getCamera().setX(-MAX_MAP_WIDTH / 2 + WIDTH);
				Window.getCamera().setY(-MAX_MAP_HEIGHT / 2 + HEIGHT);
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 - 400, Player.getTeam()));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 - 400, MAX_MAP_HEIGHT / 2 - 200));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 + 400, MAX_MAP_HEIGHT / 2 - 200));
				
				// AI
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 400, getTeamByName("red")));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 - 400, MAX_MAP_HEIGHT / 2 + 200));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 + 400, MAX_MAP_HEIGHT / 2 + 200));
				break;
				
			// map 4
			case 4:
				Window.getCamera().setX(-MAX_MAP_WIDTH / 2 + WIDTH);
				Window.getCamera().setY(-MAX_MAP_HEIGHT / 2 + HEIGHT);
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 - 200, MAX_MAP_HEIGHT / 2 - 200));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 - 400, MAX_MAP_HEIGHT / 2 - 400, Player.getTeam()));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 - 600, MAX_MAP_HEIGHT / 2 - 600));
				
				// AI
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 + 200, MAX_MAP_HEIGHT / 2 - 200));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 + 400, MAX_MAP_HEIGHT / 2 - 400, getTeamByName("red")));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2 + 600, MAX_MAP_HEIGHT / 2 - 600));
				
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 200));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 480, getTeamByName("yellow")));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 760));
				break;
				
			// map 5
			//case 5:
				//break;

			default:
				Window.getCamera().setX(-MAX_MAP_WIDTH / 2 + WIDTH);
				Window.getCamera().setY(-MAX_MAP_HEIGHT / 2 + HEIGHT);
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2, Player.getTeam()));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 250));
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 500));
				// AI
				handler.addObject(new Sun(MAX_MAP_WIDTH / 2, MAX_MAP_HEIGHT / 2 + 750, getTeamByName("red")));
				break;
			}
			Game.setGameState(Game.GAME_STATE_RUNNING);
			
		}
	}
	
	public static void resetGame() {
		// copy of the original
		LinkedList<Entity> ents = new LinkedList<Entity>(handler.getEntities());
		for (int i = 0; i < ents.size(); i++) {
			Entity ent = ents.get(i);
			if (ent.getClassName().equals("entity_sun") || ent.getClassName().equals("entity_unit")) {
				handler.removeObject(ent);
				ent = null;
			}
		}
		MapGrid.clearGrid();
		WinCondition.resetTracking();
	}
	
	public static void setActiveMap(int map) {
		Game.activeMap = map;
	}
	
	public static int getActiveMap() {
		return Game.activeMap;
	}
	
	public static GameHandler getHandler() {
		return Game.handler;
	}
	
	public void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public static void stop() {
		System.exit(0);
	}
	
	/*
	 * main game loop
	 * tries to maintain previously set maxTPS default: 60
	 * renders and ticks the game
	 */
	public void run() {
		long lastTime = System.nanoTime();
		double amountOfTicks = this.PREFERRED_TPS * Game.gameScale;
		
		// tick length, delta accounts for fluctuations
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		
		// counters for fps and tps
		int frames = 0;
		int ticks = 0;
		while(running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while(delta >= 1) {
				tick();
				SelectionBox.tick();
				delta--;
				ticks++;
			}
			render();
			frames++;
			
			if(System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				setFPS(frames); // frames per second
				setTPS(ticks); // ticks per second
				setFrameTime((double) 1 / (double) frames); // average time it took to render each frame in the last second
				ticks = 0;
				frames = 0;
			}
		}
		stop();
	}
	
	public static void registerTeam(Team team) {
		teams.add(team);
	}
	
	public static Team getTeamByName(String teamName) {
		for (int i = 0; i < teams.size(); i++) {
			if (teams.get(i).getName().equals(teamName)) {
				return teams.get(i);
			}	
		}
		return null;
	}
	
	public static void toggleGameMenu() {
		if (Game.gamestate == Game.GAME_STATE_RUNNING || Game.gamestate == Game.GAME_STATE_PAUSED) {
			Game.setGameState(Game.GAME_STATE_GAME_MENU);
		} else if (Game.gamestate == Game.GAME_STATE_GAME_MENU) {
			Game.setGameState(Game.GAME_STATE_RUNNING);
		}
	}
	
	public static void toggleRunningGame() {
		if (Game.gamestate == Game.GAME_STATE_RUNNING)
			Game.setGameState(Game.GAME_STATE_PAUSED);
		else if (Game.gamestate == GAME_STATE_PAUSED) 
			Game.setGameState(Game.GAME_STATE_RUNNING);
		else if (Game.gamestate == Game.GAME_STATE_GAME_MENU)
			Game.setGameState(Game.GAME_STATE_RUNNING);
	}
	
	public static boolean mainMenu() {
		switch (Game.gamestate) {
		case Game.GAME_STATE_MAIN_MENU:
			return true;
		case Game.GAME_STATE_MAP_SELECT:
			return true;
		case Game.GAME_STATE_COLOUR_SELECT:
			return true;
		default:
			return false;
		}
	}
	
	public static boolean menuState() {
		switch (Game.gamestate) {
		case Game.GAME_STATE_MAIN_MENU:
			return true;
		case Game.GAME_STATE_GAME_MENU:
			return true;
		case Game.GAME_STATE_LOSS:
			return true;
		case Game.GAME_STATE_WIN:
			return true;
		case Game.GAME_STATE_LOGIN:
			return true;
		case Game.GAME_STATE_SETTINGS:
			return true;
		case Game.GAME_STATE_MAP_SELECT:
			return true;
		case Game.GAME_STATE_COLOUR_SELECT:
			return true;
		default:
			return false;
		}
	}
	
	public static boolean activeGame() {
		return (Game.gamestate == Game.GAME_STATE_RUNNING || Game.gamestate == Game.GAME_STATE_PAUSED || Game.gamestate== Game.GAME_STATE_GAME_MENU) ? true : false;
	}
	
	public static boolean gameRunning() {
		return (Game.gamestate == Game.GAME_STATE_RUNNING) ? true : false;
	}
	
	public static boolean gamePaused() {
		return (Game.gamestate == Game.GAME_STATE_PAUSED) ? true : false;
	}
	
	public static int getGameState() {
		return Game.gamestate;
	}
	
	public static void setGameState(int gamestate) {
		Game.gamestate = gamestate;
		System.out.println(Game.getGamestateString());
	}
	
	// for debug
	public static String getGamestateString() {
		switch (Game.getGameState()) {
		case Game.GAME_STATE_MAIN_MENU:
			return "GAME_STATE_MAIN_MENU";
		case Game.GAME_STATE_RUNNING:
			return "GAME_STATE_RUNNING";
		case Game.GAME_STATE_PAUSED:
			return "GAME_STATE_PAUSED";
		case Game.GAME_STATE_COLOUR_SELECT:
			return "GAME_STATE_COLOUR_SELECT";
		case Game.GAME_STATE_LOGIN:
			return "GAME_STATE_LOGIN";
		case Game.GAME_STATE_LOSS:
			return "GAME_STATE_LOSS";
		case Game.GAME_STATE_MAP_SELECT:
			return "GAME_STATE_MAP_SELECT";
		case Game.GAME_STATE_SETTINGS:
			return "GAME_STATE_SETTINGS";
		case Game.GAME_STATE_WIN:
			return "GAME_STATE_WIN";
		case Game.GAME_STATE_GAME_MENU:
			return "GAME_STATE_GAME_MENU";
		default:
			return "THIS IS VERY BAD";
		}
	}
	
	public boolean getRunning() {
		return Game.running;
	}
	
	public void setFPS(int fps) {
		Game.fps = fps;
	}
	
	public static int getFPS() {
		return fps;
	}
	
	public void setTPS(int tps) {
		Game.tps = tps;
	}
	
	public static int getTPS() {
		return tps;
	}
	
	public void setFrameTime(double frametime) {
		Game.frametime = frametime;
	}
	
	public static double getFrameTime() {
		return frametime;
	}
	
	private void tick() {
		handler.tick();
	}
	
	private void render() {
		window.renderGame(this, renderHandler);
	}
	
	public static GUIHandler getGuihandler() {
		return guihandler;
	}
	
	// utility function to get the aspect ratio from the monitor's width and height
	// only used to set the original window size
	private static Dimension getAspectRatio() {
		double ratio = Game.screensize.getWidth() / Game.screensize.getHeight();
		double bestDelta = Double.MAX_VALUE;
		int width = 0;
		int height = 0;

		for (int i = 1; i < 50; i++) {
		  for (int j = 1; j < 50; j++) {
		    double newDelta = Math.abs((double) i / (double) j - ratio);
		    if (newDelta < bestDelta) {
		      bestDelta = newDelta;
		      width = i;
		      height = j;
		    }
		  }
		}
		return new Dimension(width, height);
	}
	
	public static void main(String[] args) {
		new Game();
	}
	
}
