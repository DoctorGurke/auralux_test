package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javax.swing.JFrame;

import model.Game;
import model.GameHandler;
import model.ents.Camera;

public class Window {
	
	private static Camera camera;
	
	private DecimalFormat df;
	
	
	public Window (int width, int height, String title, Game game, Camera camera) {
		Window.camera = camera;
		Game.getHandler().addObject(camera);
		
		JFrame frame = new JFrame(title);
		// we really don't want the window resized ok. No time for pesky rescale
		frame.setPreferredSize(new Dimension(width, height));
		frame.setMaximumSize(new Dimension(width, height));
		frame.setMinimumSize(new Dimension(width, height));
		frame.setResizable(false);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.add(game);
		frame.setVisible(true);
		frame.setBackground(Color.black);
		
		df = new DecimalFormat("#.####");
		df.setRoundingMode(RoundingMode.CEILING);
		
		game.start();
	}
	
	public static Camera getCamera() {
		return Window.camera;
	}
	
	public void renderGame(Game game, RenderHandler renderHandler) {
		BufferStrategy bufferStrat = game.getBufferStrategy();
		if (bufferStrat == null) {
			// triple buffer strategy for rendering pipeline
			game.createBufferStrategy(3);
			return;
		}
		
		// our drawing context. Learn to love it
		Graphics2D surface = (Graphics2D) bufferStrat.getDrawGraphics();
		
		// "clear" or reset canvas each frame to draw on
		surface.setColor(Color.black);
		surface.fillRect(0, 0, game.getWidth(), game.getHeight());
		
		// only render actually on screen
		surface.setClip(0, 0, game.getWidth(), game.getHeight());
		
		
		// enable antialiasing and pure stroke
		surface.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		surface.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		
		// set default font for drawString for future drawing
		surface.setFont(new Font("Electrolize", Font.PLAIN, 25));
		
		//surface.scale(2, 2);
		
		// renders everything that's part of the game. camera offset is passed to account for it's movement
		// when rendering, if needed
		
		renderHandler.render(surface, camera.getOffsetX(), camera.getOffsetY());
		
		// constant FPS and TPS numbers in the top left corner
		// TODO: make the box resize with the text? (EFFORT EW)
		surface.setFont(new Font("Electrolize", Font.PLAIN, 13));
		surface.setColor(new Color(50, 50, 50, 150));
		surface.fillRect(0, 0, 75, 60);
		surface.setColor(Color.green);
		surface.drawString("FPS: " + Game.getFPS(), 0, 10);
		surface.drawString("FT: " + df.format(Game.getFrameTime()), 0, 25);
		surface.drawString("TPS: " + Game.getTPS(), 0, 40);
		surface.drawString("ENTS: " + GameHandler.numEntities, 0, 55);
		
		surface.dispose();
		bufferStrat.show();
	}
	
}
